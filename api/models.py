from django.db import models
from django.utils import timezone


class Category(models.Model):
    name = models.TextField()
    sort_order = models.IntegerField()
    icon_url = models.TextField()


class Cuisine(models.Model):
    cuisine = models.TextField(null=True)


class Tag(models.Model):
    tag = models.TextField(null=True)


class Recipe(models.Model):
    image_uri = models.TextField(null=True)
    video_uri = models.TextField(null=True)
    name = models.TextField(null=True)
    server_id = models.IntegerField()
    thumb_uri = models.TextField(null=True)
    difficulty_id = models.IntegerField()
    rating = models.TextField(null=True)
    cook_time = models.TextField(null=True)
    cuisine = models.ForeignKey(Cuisine, on_delete=models.CASCADE, related_name='recipes')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='recipes')
    tags = models.ManyToManyField(Tag, through='TagRecipe')


class TagRecipe(models.Model):
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('recipe', 'tag')
        db_table = 'api_recipe_tag'


class Ingredient(models.Model):
    ingr = models.TextField()
    count = models.TextField(null=True)
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE, related_name='ingredients')


class Instruction(models.Model):
    step = models.TextField(null=True)
    step_img = models.TextField(null=True)
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE, related_name='instructions')


class Nutrition(models.Model):
    title = models.TextField(null=True)
    weight = models.TextField(null=True)
    protein = models.TextField(null=True)
    fat = models.TextField(null=True)
    carbohydrate = models.TextField(null=True)
    calorific_value = models.TextField(null=True)
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE, related_name='nutrition_list')


class ViewCount(models.Model):
    server_id = models.IntegerField()
    view_count = models.IntegerField()


class UserToken(models.Model):
    token = models.TextField()
    date = models.DateTimeField(default=timezone.now)
