from django.contrib import admin
from django.contrib.auth.models import Group, User

from api.models import *

admin.site.register(Category)
admin.site.register(Cuisine)
admin.site.register(Tag)
admin.site.register(Recipe)
admin.site.register(TagRecipe)
admin.site.register(Ingredient)
admin.site.register(Instruction)
admin.site.register(Nutrition)
admin.site.register(ViewCount)

admin.site.unregister(Group)
admin.site.unregister(User)
