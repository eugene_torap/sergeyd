import json

from django.core import serializers
from django.db import transaction
from django.http import JsonResponse

from api.models import Tag, Cuisine, Category, Ingredient, Instruction, Nutrition, ViewCount, UserToken
from api.serializers import RecipesSerializer, Recipe, TestRecipesSerializer, IngredientSerializer, \
    InstructionSerializer, NutritionSerializer, TagSerializer, ViewCountSerializer
from django.views import View
from django.db.models import F
from django.utils import timezone

from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response


class RecipeView(viewsets.ViewSet):
    queryset = Recipe.objects.all()
    serializer_class = RecipesSerializer

    @action(detail=False)
    def get(self, request, pk=None):
        recipe_count = request.GET['recipe_count']

        if not recipe_count.isdigit():
            return JsonResponse({'error': 'Bad Request'}, status=400)

        recipe_count = int(recipe_count)
        token = request.GET.get('token')
        if not check_token(token):
            return JsonResponse({'error': 'Authorization Required'}, status=401)

        limit = recipe_count + 100
        recipes = Recipe.objects.select_related('cuisine', 'category').prefetch_related('ingredients', 'instructions', 'nutrition_list', 'tags')[recipe_count:limit]
        serializer = RecipesSerializer(recipes, many=True)
        return Response(serializer.data)

    @action(detail=False, methods=['post'])
    def add(self, request):
        recipe_items = request.data['recipe_items']

        exist_recipes = set(Recipe.objects.values_list('server_id', flat=True))

        recipes = json.loads(recipe_items)
        object_recipes = []
        recipe_serializer = TestRecipesSerializer()

        server_ids = set()
        for recipe in recipes:
            server_ids.add(recipe['server_id'])
            object_recipes.append(recipe_serializer.create(recipe))

        delete_recipes = list(server_ids.intersection(exist_recipes))
        tag_serializer = TagSerializer()
        ingredient_serializer = IngredientSerializer()
        instruction_serializer = InstructionSerializer()
        nutrition_serializer = NutritionSerializer()
        with transaction.atomic():
            if delete_recipes:
                Recipe.objects.filter(server_id__in=delete_recipes).delete()

            Recipe.objects.bulk_create(object_recipes)
            object_ingredients = []
            object_instructions = []
            object_nutrition_list = []
            for index, recipe in enumerate(recipes):
                tags = tag_serializer.create(recipe)
                if tags:
                    object_recipes[index].tags.set(tags)

                ingredient = ingredient_serializer.create_all(recipe, object_recipes[index])
                if ingredient:
                    object_ingredients.extend(ingredient)

                instruction = instruction_serializer.create_all(recipe, object_recipes[index])
                if instruction:
                    object_instructions.extend(instruction)

                nutrition = nutrition_serializer.create_all(recipe, object_recipes[index])
                if nutrition:
                    object_nutrition_list.extend(nutrition)

            Ingredient.objects.bulk_create(object_ingredients)
            Instruction.objects.bulk_create(object_instructions)
            Nutrition.objects.bulk_create(object_nutrition_list)

        return JsonResponse({}, status=200)

    @action(detail=False)
    def search(self, request, pk=None):
        name = request.GET['q']

        token = request.GET.get('token')
        if not check_existed_token(token):
            return JsonResponse({'error': 'Authorization Required'}, status=401)

        recipes = Recipe.objects.filter(name__icontains=name).select_related('cuisine', 'category').prefetch_related('ingredients', 'instructions', 'nutrition_list', 'tags')[:40]
        serializer = RecipesSerializer(recipes, many=True)
        return Response(serializer.data)


class VisitView(View):
    def get(self, request):
        server_id = request.GET['server_id']

        if not server_id.isdigit():
            return JsonResponse({'error': 'Bad Request'}, status=400)

        server_id = int(server_id)
        token = request.GET.get('token')
        if not check_existed_token(token):
            return JsonResponse({'error': 'Authorization Required'}, status=401)

        view_count = ViewCount.objects.filter(server_id=server_id).first()
        if not view_count:
            view_count = ViewCount(server_id=server_id, view_count=0)

        serializer = ViewCountSerializer(view_count)
        return JsonResponse(serializer.data, status=200)

    def post(self, request):
        server_id = request.POST['server_id']

        if not server_id.isdigit():
            return JsonResponse({'error': 'Bad Request'}, status=400)

        server_id = int(server_id)
        token = request.POST.get('token')
        if not check_existed_token(token):
            return JsonResponse({'error': 'Authorization Required'}, status=401)

        view_count = ViewCount.objects.filter(server_id=server_id)
        if view_count:
            view_count.update(view_count=F('view_count') + 1)
        else:
            view_count = ViewCount(server_id=server_id, view_count=1)
            view_count.save()
        return JsonResponse({}, status=200)


def check_existed_token(token):
    if not token:
        return False

    user_token = UserToken.objects.filter(token=token).first()
    if not user_token:
        user_token = UserToken(token=token)
        user_token.save()

    return True


def check_token(token):
    if not token:
        return False

    user_token = UserToken.objects.filter(token=token).first()
    if user_token:
        token_date = user_token.date
        time_difference = timezone.now() - token_date
        if time_difference.days > 0:
            UserToken.objects.filter(token=token).update(date=timezone.now())
            return True
    else:
        user_token = UserToken(token=token)
        user_token.save()
        return True
    return False
