from rest_framework import serializers
from .models import *


class NutritionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Nutrition
        fields = ('title', 'weight', 'protein', 'fat', 'carbohydrate', 'calorific_value')

    @staticmethod
    def create_all(validated_data, recipe):
        nutrition_list = validated_data.get('nutrition_list')
        if not nutrition_list:
            return None

        object_nutrition_list = []
        for nutrition in nutrition_list:
            title = nutrition.get('title')
            weight = nutrition.get('weight')
            protein = nutrition.get('protein')
            fat = nutrition.get('fat')
            carbohydrate = nutrition.get('carbohydrate')
            calorific_value = nutrition.get('calorific_value')
            object_nutrition_list.append(Nutrition(title=title, weight=weight, protein=protein, fat=fat, recipe=recipe,
                                                   carbohydrate=carbohydrate, calorific_value=calorific_value))

        return object_nutrition_list


class InstructionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Instruction
        fields = ('step', 'step_img')

    @staticmethod
    def create_all(validated_data, recipe):
        instructions = validated_data.get('instructions')
        if not instructions:
            return None

        object_instructions = []
        for instruction in instructions:
            step = instruction.get('step')
            step_img = instruction.get('step_img')
            object_instructions.append(Instruction(step=step, step_img=step_img, recipe=recipe))

        return object_instructions


class IngredientSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Ingredient
        fields = ('ingr', 'count')

    @staticmethod
    def create_all(validated_data, recipe):
        ingredients = validated_data.get('ingredients')
        if not ingredients:
            return None

        object_ingredients = []
        for ingredient in ingredients:
            ingr = ingredient.get('ingr')
            count = ingredient.get('count')
            object_ingredients.append(Ingredient(ingr=ingr, count=count, recipe=recipe))

        return object_ingredients


class TagSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Tag
        fields = ('tag', 'id')

    def create(self, validated_data):
        tag_ids = validated_data.get('tags')
        if not tag_ids:
            return None

        return tag_ids


class CuisineSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Cuisine
        fields = ('cuisine', 'id')


class RecipesSerializer(serializers.HyperlinkedModelSerializer):
    instructions = InstructionSerializer(many=True, read_only=True)
    ingredients = IngredientSerializer(many=True, read_only=True)
    nutrition_list = NutritionSerializer(many=True, read_only=True)
    tags = TagSerializer(many=True, read_only=True)
    cuisine = CuisineSerializer(many=False, read_only=True)

    class Meta:
        model = Recipe
        fields = ('image_uri', 'video_uri', 'name', 'server_id', 'thumb_uri', 'difficulty_id', 'rating', 'cook_time',
                  'category_id', 'cuisine', 'instructions', 'ingredients', 'nutrition_list', 'tags')


class TestRecipesSerializer(serializers.Serializer):
    def create(self, validated_data):
        image_uri = validated_data.get('image_uri')
        video_uri = validated_data.get('video_uri')
        name = validated_data.get('name')
        server_id = validated_data.get('server_id')
        thumb_uri = validated_data.get('thumb_uri')
        difficulty_id = validated_data.get('difficulty_id')
        rating = validated_data.get('rating')
        cook_time = validated_data.get('cook_time')
        cuisine_id = validated_data.get('cuisine_id')
        category_id = validated_data.get('category_id')

        return Recipe(image_uri=image_uri, video_uri=video_uri, name=name, server_id=server_id, thumb_uri=thumb_uri,
                      difficulty_id=difficulty_id, rating=rating, cook_time=cook_time, cuisine_id=cuisine_id,
                      category_id=category_id)


class ViewCountSerializer(serializers.ModelSerializer):
    class Meta:
        model = ViewCount
        fields = ("server_id", "view_count")
